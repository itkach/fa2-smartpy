# FA2
# ===
#
# Multi-asset contract.
#
# Cf. https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-12/fa2_interface.mligo
#
# WARNING: This script requires the /dev version of SmartPy.
#

import smartpy as sp


################################################################################
# Global Parameters
#
def global_parameter(env_var, default):
    try:
        if os.environ[env_var] == "true" :
            return True
        if os.environ[env_var] == "false" :
            return False
        return default
    except:
        return default


debug_mode = global_parameter("debug_mode", False)
# Use maps instead of big-maps and things like that (i.e. make
# inspection of the state easier).

single_asset = global_parameter("single_asset", False)
# Make the contract work only for token-id 0

readable = global_parameter("carthage_pairs", True)
# User-accounts are big-maps: (user-address * token-id) -> ownership-info
#
# For Babylon, one should use `readable = False` to use `PACK` on the pair,
# we keep this option around for benchmarking purposes.
#

force_layouts = global_parameter("force_layouts", False)
# The spec requires records and variants to be right-combs; we keep
# this parameter around to be able to compare performance & code-size.

support_operator = global_parameter("support_operator", True)
# The operator entry-points always have to be there, but there is
# definitely a use-case for having them completely empty (saving
# storage and gas when `support_operator` is `False).

assume_consecutive_token_ids = global_parameter(
    "assume_consecutive_token_ids", True)
# For a previous version of the TZIP specification, it was necessary to keep
# track of the set of all tokens in the contract.
# The set of tokens is for now still available; this parameter guides how to
# implement it:
# If true we don't need a set of token ids, just to know how many there are.

add_mutez_transfer = global_parameter("add_mutez_transfer", True)
# Add an entry point for the administrator to transfer tez potentially
# in the contract's balance.

add_permissions_descriptor = global_parameter("add_permissions_descriptor", True)
# Add the `permissions_descriptor` entry-point; it is part of the specification
# but costs gas and storage so we keep the option of not adding it.


if debug_mode:
    my_map = sp.map
else:
    my_map = sp.big_map

################################################################################

token_id_type = sp.TNat


class Error_message:
    def token_undefined(): return "TOKEN_UNDEFINED"
    def insufficient_balance(): return "INSUFFICIENT_BALANCE"
    def not_operator(): return "NOT_OPERATOR"
    def not_owner(): return "NOT_OWNER"

# This is the new type from the spec:
#
#     type transfer = {
#       from_ : address;
#       txs: {
#         to_ : address;
#         token_id : token_id;
#         amount : nat;
#       } list
#     } list
#
class Batch_transfer:
    def get_transfer_type():
        tx_type = sp.TRecord(to_ = sp.TAddress,
                             token_id = token_id_type,
                             amount = sp.TNat)
        if force_layouts:
            tx_type = tx_type.layout(
                ("to_", ("token_id", "amount"))
            )
        transfer_type = sp.TRecord(from_ = sp.TAddress,
                                   txs = sp.TList(tx_type)).layout(
                                       ("from_", "txs"))
        return transfer_type
    def get_type():
        return sp.TList(Batch_transfer.get_transfer_type())
    def item(from_, txs):
        v = sp.record(from_ = from_, txs = txs)
        sp.set_type(v, Batch_transfer.get_transfer_type())
        return v


class Operator_param:
    def get_type():
        return sp.TRecord(
            owner = sp.TAddress,
            operator = sp.TAddress)
    def set_type_and_layout(expr):
        sp.set_type(expr, Operator_param.get_type())
        if force_layouts:
            sp.set_record_layout(expr, ("owner", "operator"))
    def make(owner, operator):
        r = sp.record(owner = owner,
                      operator = operator)
        Operator_param.set_type_and_layout(r)
        return r
    def is_operator_response_type():
        return sp.TRecord(
            operator = Operator_param.get_type(),
            is_operator = sp.TBool)
    def make_is_operator_response(operator, is_operator):
        return sp.record(operator = operator, is_operator = is_operator)
    def is_operator_request_type():
        return sp.TRecord(
            operator = Operator_param.get_type(),
            callback = sp.TContract(Operator_param.is_operator_response_type()))

class Ledger_value:
    def get_type():
        return sp.TRecord(balance = sp.TNat)
    def make(balance):
        return sp.record(balance = balance)

class Ledger_key:
    def make(user, token):
        sp.set_type(user, sp.TAddress)
        sp.set_type(token, token_id_type)
        if single_asset:
            result = user
        else:
            result = sp.pair(user, token)
        if readable:
            return result
        else:
            return sp.pack(result)

class Operator_set:
    def key_type():
        if readable:
            t = sp.TRecord(owner = sp.TAddress,
                           operator = sp.TAddress).layout(("owner", "operator"))
            return t
        else:
            return sp.TBytes
    def make():
        return my_map(tkey = Operator_set.key_type(), tvalue = sp.TUnit)
    def make_key(owner, operator):
        metakey = sp.record(owner = owner, operator = operator)
        if readable:
            return metakey
        else:
            return sp.pack(metakey)
    def add(set, owner, operator):
        set[Operator_set.make_key(owner, operator)] = sp.unit
    def remove(set, owner, operator):
        del set[Operator_set.make_key(owner, operator)]
    def is_member(set, owner, operator):
        return set.contains(Operator_set.make_key(owner, operator))

class Balance_of:
    def request_type():
        return sp.TRecord(
            owner = sp.TAddress,
            token_id = token_id_type)
    def response_type():
        return sp.TList(
            sp.TRecord(
                request = Balance_of.request_type(),
                balance = sp.TNat))

class Total_supply:
    def request_type():
        return token_id_type
    def response_type():
        return sp.TList(
            sp.TRecord(
                token_id = token_id_type,
                total_supply = sp.TNat))

class Token_meta_data:
    def get_type():
        return sp.TRecord(
            token_id = token_id_type,
            symbol = sp.TString,
            name = sp.TString,
            decimals = sp.TNat,
            extras = sp.TMap(sp.TString, sp.TString)
        )
    def set_type_and_layout(expr):
        sp.set_type(expr, Token_meta_data.get_type())
        if force_layouts:
            sp.set_record_layout(expr, ("token_id",
                                        ("symbol",
                                         ("name",
                                          ("decimals", "extras")))))
    def request_type():
        return Total_supply.request_type()

class Permissions_descriptor:
    def get_type():
        operator_transfer_policy = sp.TVariant(
            no_transfer = sp.TUnit,
            owner_transfer = sp.TUnit,
            owner_or_operator_transfer = sp.TUnit)
        if force_layouts:
            sp.set_type_variant_layout(operator_transfer_policy,
                                       ("no_transfer",
                                        ("owner_transfer",
                                         "owner_or_operator_transfer")))
        owner_transfer_policy =  sp.TVariant(
            owner_no_op = sp.TUnit,
            optional_owner_hook = sp.TUnit,
            required_owner_hook = sp.TUnit)
        if force_layouts:
            sp.set_type_variant_layout(owner_transfer_policy,
                                       ("owner_no_op",
                                        ("optional_owner_hook",
                                         "required_owner_hook")))
        custom_permission_policy = sp.TRecord(
            tag = sp.TString,
            config_api = sp.TOption(sp.TAddress))
        main = sp.TRecord(
            operator = operator_transfer_policy,
            receiver = owner_transfer_policy,
            sender   = owner_transfer_policy,
            custom   = sp.TOption(custom_permission_policy))
        if force_layouts:
            sp.set_type_record_layout(main, ("operator",
                                             ("receiver",
                                              ("sender", "custom"))))
        return main
    def set_type_and_layout(expr):
        sp.set_type(expr, Permissions_descriptor.get_type())
    def make():
        def uv(s):
            return sp.variant(s, sp.unit)
        v = sp.record(
            operator =
                uv("owner_or_operator_transfer")
                if support_operator else
                    uv("owner_transfer"),
            receiver = uv("owner_no_op"),
            sender = uv("owner_no_op"),
            custom = sp.none
            )
        Permissions_descriptor.set_type_and_layout(v)
        return v

class Token_id_set:
    def empty():
        if assume_consecutive_token_ids:
            # The "set" is its cardinal.
            return sp.nat(0)
        else:
            return sp.set(t = token_id_type)
    def add(metaset, v):
        if assume_consecutive_token_ids:
            metaset.set(sp.max(metaset, v + 1))
        else:
            metaset.add(v)

def mutez_transfer(contract, params):
    sp.verify(sp.sender == contract.data.administrator)
    sp.set_type(params.destination, sp.TAddress)
    sp.set_type(params.amount, sp.TMutez)
    sp.send(params.destination, params.amount)

def permissions_descriptor(contract, params):
    sp.set_type(params, sp.TContract(Permissions_descriptor.get_type()))
    v = Permissions_descriptor.make()
    sp.transfer(v, sp.mutez(0), params)

class FA2(sp.Contract):
    def __init__(self, admin):
        if  add_mutez_transfer:
            self.transfer_mutez = sp.entry_point(mutez_transfer)
        if  add_permissions_descriptor:
            self.permissions_descriptor = sp.entry_point(permissions_descriptor)
        self.init(
            paused = False,
            ledger =
                my_map(tvalue = Ledger_value.get_type()),
            tokens =
               my_map(tvalue = sp.TRecord(
                    total_supply = sp.TNat,
                    metadata = Token_meta_data.get_type()
                )),
            operators = Operator_set.make(),
            administrator = admin,
            all_tokens = Token_id_set.empty(),
            # This field is a placeholder, the build system will replace
            # the annotation with a version-string:
            metadata_string = sp.unit
        )

    @sp.entry_point
    def set_pause(self, params):
        sp.verify(sp.sender == self.data.administrator)
        self.data.paused = params

    @sp.entry_point
    def set_administrator(self, params):
        sp.verify(sp.sender == self.data.administrator)
        self.data.administrator = params

    @sp.entry_point
    def mint(self, params):
        sp.verify(sp.sender == self.data.administrator)
        # We don't check for pauseness because we're the admin.
        if single_asset:
            sp.verify(params.token_id == 0, "single-asset: token-id <> 0")
        user = Ledger_key.make(params.address, params.token_id)
        Token_id_set.add(self.data.all_tokens, params.token_id)
        sp.if self.data.ledger.contains(user):
            self.data.ledger[user].balance += params.amount
        sp.else:
            self.data.ledger[user] = Ledger_value.make(params.amount)
        sp.if self.data.tokens.contains(params.token_id):
             self.data.tokens[params.token_id].total_supply += params.amount
        sp.else:
             self.data.tokens[params.token_id] = sp.record(
                 total_supply = params.amount,
                 metadata = sp.record(
                     token_id = params.token_id,
                     symbol = params.symbol,
                     name = "", # Consered useless here
                     decimals = 0,
                     extras = sp.map()
                 )
             )

    @sp.entry_point
    def transfer(self, params):
        sp.verify( ~self.data.paused )
        sp.set_type(params, Batch_transfer.get_type())
        sp.for transfer in params:
           current_from = transfer.from_
           if support_operator:
               sp.verify(
                   (sp.sender == self.data.administrator) |
                   (current_from == sp.sender) |
                   Operator_set.is_member(self.data.operators,
                                          current_from,
                                          sp.sender),
                   message = Error_message.not_operator())
           else:
               sp.verify(
                   (sp.sender == self.data.administrator) |
                   (current_from == sp.sender),
                   message = Error_message.not_owner())
           sp.for tx in transfer.txs:
                sp.verify(tx.amount > 0, message = "TRANSFER_OF_ZERO")
                if single_asset:
                    sp.verify(tx.token_id == 0, "single-asset: token-id <> 0")
                from_user = Ledger_key.make(current_from, tx.token_id)
                sp.verify(
                    (self.data.ledger[from_user].balance >= tx.amount),
                    message = Error_message.insufficient_balance())
                to_user = Ledger_key.make(tx.to_, tx.token_id)
                self.data.ledger[from_user].balance = sp.as_nat(
                    self.data.ledger[from_user].balance - tx.amount)
                sp.if self.data.ledger.contains(to_user):
                    self.data.ledger[to_user].balance += tx.amount
                sp.else:
                     self.data.ledger[to_user] = Ledger_value.make(tx.amount)

    @sp.entry_point
    def balance_of(self, params):
        # paused may mean that balances are meaningless:
        sp.verify( ~self.data.paused )
        res = sp.local("responses", [])
        sp.set_type(res.value, Balance_of.response_type())
        sp.for req in params.requests:
            user = Ledger_key.make(req.owner, req.token_id)
            balance = self.data.ledger[user].balance
            res.value.push(
                sp.record(
                    request = sp.record(
                        owner = sp.set_type(req.owner, sp.TAddress),
                        token_id = sp.set_type(req.token_id, sp.TNat)),
                    balance = balance))
        destination = sp.set_type(params.callback,
                                  sp.TContract(Balance_of.response_type()))
        sp.transfer(res.value.rev(), sp.mutez(0), destination)

    @sp.entry_point
    def total_supply(self, params):
        sp.verify( ~self.data.paused )
        res = sp.local("responses", [])
        sp.set_type(res.value, Total_supply.response_type())
        sp.for req in params.token_ids:
            res.value.push(
                sp.record(
                    token_id = req,
                    total_supply = self.data.tokens[req].total_supply))
        destination = sp.set_type(params.callback,
                                  sp.TContract(Total_supply.response_type()))
        sp.transfer(res.value.rev(), sp.mutez(0), destination)

    @sp.entry_point
    def token_metadata(self, params):
        sp.verify( ~self.data.paused )
        res = sp.local("responses", [])
        sp.for req in params.token_ids:
            Token_meta_data.set_type_and_layout(self.data.tokens[req].metadata)
            res.value.push(self.data.tokens[req].metadata)
        destination = sp.set_type(params.callback,
                                  sp.TContract(
                                      sp.TList(Token_meta_data.get_type())))
        sp.transfer(res.value.rev(), sp.mutez(0), destination)

    @sp.entry_point
    def update_operators(self, params):
        sp.set_type(params, sp.TList(
            sp.TVariant(
                add_operator = Operator_param.get_type(),
                remove_operator = Operator_param.get_type())))
        if support_operator:
            sp.for update in params:
                sp.if update.is_variant("add_operator"):
                    upd = update.open_variant("add_operator")
                    sp.verify((upd.owner == sp.sender) |
                              (sp.sender == self.data.administrator))
                    Operator_set.add(self.data.operators,
                                     upd.owner,
                                     upd.operator)
                sp.else:
                    upd = update.open_variant("remove_operator")
                    sp.verify((upd.owner == sp.sender) |
                              (sp.sender == self.data.administrator))
                    Operator_set.remove(self.data.operators,
                                        upd.owner,
                                        upd.operator)
        else:
            sp.failwith("not implemented")


    @sp.entry_point
    def is_operator(self, params):
        sp.set_type(params, Operator_param.is_operator_request_type())
        if support_operator:
            res = Operator_set.is_member(self.data.operators,
                                         params.operator.owner,
                                         params.operator.operator)
            returned = sp.record(
                operator = params.operator,
                is_operator = res)
            sp.transfer(returned, sp.mutez(0), params.callback)
        else:
            returned = sp.record(
                operator = params.operator,
                is_operator = False)
            sp.transfer(returned, sp.mutez(0), params.callback)

class View_consumer(sp.Contract):
    def __init__(self):
        self.init(last_sum = 0,
                  last_acc = "",
                  last_operator = True,
                  operator_support =  False if support_operator else True)

    @sp.entry_point
    def receive_balances(self, params):
        sp.set_type(params, Balance_of.response_type())
        self.data.last_sum = 0
        sp.for resp in params:
            self.data.last_sum += resp.balance

    @sp.entry_point
    def receive_total_supplies(self, params):
        sp.set_type(params, Total_supply.response_type())
        self.data.last_sum = 0
        sp.for resp in params:
            self.data.last_sum += resp.total_supply

    @sp.entry_point
    def receive_metadata(self, params):
        self.data.last_acc = ""
        sp.for resp in params:
            Token_meta_data.set_type_and_layout(resp)
            self.data.last_acc += resp.symbol

    @sp.entry_point
    def receive_is_operator(self, params):
        sp.set_type(params, Operator_param.is_operator_response_type())
        self.data.last_operator = params.is_operator

    @sp.entry_point
    def receive_permissions_descriptor(self, params):
        Permissions_descriptor.set_type_and_layout(params)
        sp.if params.operator.is_variant("owner_or_operator_transfer"):
            self.data.operator_support = True
        sp.else:
            self.data.operator_support = False

def arguments_for_balance_of(receiver, reqs):
    return (sp.record(
        callback = sp.contract(Balance_of.response_type(),
                               sp.contract_address(receiver),
                               entry_point = "receive_balances").open_some(),
        requests = reqs))


if "templates" not in __name__:
    @sp.add_test(name = "FA2")
    def test():
        scenario = sp.test_scenario()
        scenario.h1("Simple FA2 Contract")
        # sp.test_account generates ED25519 key-pairs deterministically:
        admin = sp.test_account("Administrator")
        alice = sp.test_account("Alice")
        bob   = sp.test_account("Robert")
        # Let's display the accounts:
        scenario.h2("Accounts")
        scenario.show([admin, alice, bob])
        c1 = FA2(admin.address)
        scenario += c1
        scenario.h2("Initial Minting")
        scenario.p("The administrator mints 100 token-0's to Alice.")
        scenario += c1.mint(address = alice.address,
                            amount = 100,
                            symbol = 'TK0',
                            token_id = 0).run(sender = admin)
        scenario.h2("Transfers Alice -> Bob")
        scenario += c1.transfer(
            [
                Batch_transfer.item(from_ = alice.address,
                                    txs = [
                                        sp.record(to_ = bob.address,
                                                  amount = 10,
                                                  token_id = 0)
                                    ])
            ]).run(sender = alice)
        scenario.verify(
            c1.data.ledger[Ledger_key.make(alice.address, 0)].balance == 90)
        scenario.verify(
            c1.data.ledger[Ledger_key.make(bob.address, 0)].balance == 10)
        scenario += c1.transfer(
            [
                Batch_transfer.item(from_ = alice.address,
                                    txs = [
                                        sp.record(to_ = bob.address,
                                                  amount = 10,
                                                  token_id = 0),
                                        sp.record(to_ = bob.address,
                                                  amount = 11,
                                                  token_id = 0)
                                    ])
            ]).run(sender = alice)
        scenario.verify(
            c1.data.ledger[Ledger_key.make(alice.address, 0)].balance
            == 90 - 10 - 11)
        scenario.verify(
            c1.data.ledger[Ledger_key.make(bob.address, 0)].balance
            == 10 + 10 + 11)
        if single_asset:
            return
        scenario.h2("More Token Types")
        scenario += c1.mint(address = bob.address,
                            amount = 100,
                            symbol = 'TK1',
                            token_id = 1).run(sender = admin)
        scenario += c1.mint(address = bob.address,
                            amount = 200,
                            symbol = 'TK2',
                            token_id = 2).run(sender = admin)
        scenario.h3("Multi-token Transfer Bob -> Alice")
        scenario += c1.transfer(
            [
                Batch_transfer.item(from_ = bob.address,
                                    txs = [
                                        sp.record(to_ = alice.address,
                                                  amount = 10,
                                                  token_id = 0),
                                        sp.record(to_ = alice.address,
                                                  amount = 10,
                                                  token_id = 1)]),
                # We voluntarily test a different sub-batch:
                Batch_transfer.item(from_ = bob.address,
                                    txs = [
                                        sp.record(to_ = alice.address,
                                                  amount = 10,
                                                  token_id = 2)])
            ]).run(sender = bob)
        scenario.h2("Other Basic Permission Tests")
        scenario.h3("Bob cannot transfer Alice's tokens.")
        scenario += c1.transfer(
            [
                Batch_transfer.item(from_ = alice.address,
                                    txs = [
                                        sp.record(to_ = bob.address,
                                                  amount = 10,
                                                  token_id = 0),
                                        sp.record(to_ = bob.address,
                                                  amount = 1,
                                                  token_id = 0)])
            ]).run(sender = bob, valid = False)
        scenario.h3("Admin can transfer anything.")
        scenario += c1.transfer(
            [
                Batch_transfer.item(from_ = alice.address,
                                    txs = [
                                        sp.record(to_ = bob.address,
                                                  amount = 10,
                                                  token_id = 0),
                                        sp.record(to_ = bob.address,
                                                  amount = 10,
                                                  token_id = 1)]),
                Batch_transfer.item(from_ = bob.address,
                                    txs = [
                                        sp.record(to_ = alice.address,
                                                  amount = 11,
                                                  token_id = 0)])
            ]).run(sender = admin)
        scenario.h3("Even Admin cannot transfer too much.")
        scenario += c1.transfer(
            [
                Batch_transfer.item(from_ = alice.address,
                                    txs = [
                                        sp.record(to_ = bob.address,
                                                  amount = 1000,
                                                  token_id = 0)])
            ]).run(sender = admin, valid = False)
        scenario.h3("Consumer Contract for Callback Calls.")
        consumer = View_consumer()
        scenario += consumer
        scenario.p("Consumer virtual address: "
                   + sp.contract_address(consumer).export())
        scenario.h2("Balance-of.")
        scenario += c1.balance_of(arguments_for_balance_of(consumer, [
            sp.record(owner = alice.address, token_id = 0),
            sp.record(owner = alice.address, token_id = 1),
            sp.record(owner = alice.address, token_id = 2)
        ]))
        scenario.verify(consumer.data.last_sum == 90)
        scenario.h2("Total Supply.")
        scenario += c1.total_supply(
            sp.record(
                callback = sp.contract(
                    Total_supply.response_type(),
                    sp.contract_address(consumer),
                    entry_point = "receive_total_supplies").open_some(),
                token_ids = [0, 1]))
        scenario.verify(consumer.data.last_sum == 200)
        scenario.h2("Token Metadata.")
        scenario += c1.token_metadata(
            sp.record(
                callback = sp.contract(
                    sp.TList(Token_meta_data.get_type()),
                    sp.contract_address(consumer),
                    entry_point = "receive_metadata").open_some(),
                token_ids = [0, 1]))
        scenario.verify(consumer.data.last_acc == "TK0TK1")
        scenario.h2("Operators")
        if not support_operator:
            scenario.h3("This version was compiled with no operator support")
            scenario.p("Calls should fail even for the administrator:")
            scenario += c1.update_operators([]).run(sender = admin, valid = False)
            scenario += c1.permissions_descriptor(
                sp.contract(
                    Permissions_descriptor.get_type(),
                    sp.contract_address(consumer),
                    entry_point = "receive_permissions_descriptor").open_some())
            scenario.verify(consumer.data.operator_support == False)
        else:
            scenario.p("This version was compiled with operator support.")
            scenario.p("Calling 0 updates should work:")
            scenario += c1.update_operators([]).run()
            scenario.h3("Operator Accounts")
            op0 = sp.test_account("Operator0")
            op1 = sp.test_account("Operator1")
            op2 = sp.test_account("Operator2")
            scenario.show([op0, op1, op2])
            scenario.p("Admin can change Alice's operator.")
            scenario += c1.update_operators([
                sp.variant("add_operator", Operator_param.make(
                    owner = alice.address,
                    operator = op1.address))
            ]).run(sender = admin)
            scenario.p("Operator1 can now transfer Alice's tokens")
            scenario += c1.transfer(
                [
                    Batch_transfer.item(from_ = alice.address,
                                        txs = [
                                            sp.record(to_ = bob.address,
                                                      amount = 2,
                                                      token_id = 0),
                                            sp.record(to_ = op1.address,
                                                      amount = 2,
                                                      token_id = 2)])
                ]).run(sender = op1)
            scenario.p("Operator1 cannot transfer Bob's tokens")
            scenario += c1.transfer(
                [
                    Batch_transfer.item(from_ = bob.address,
                                        txs = [
                                            sp.record(to_ = op1.address,
                                                      amount = 2,
                                                      token_id = 1)])
                ]).run(sender = op1, valid = False)
            scenario.p("Operator2 cannot transfer Alice's tokens")
            scenario += c1.transfer(
                [
                    Batch_transfer.item(from_ = alice.address,
                                        txs = [
                                            sp.record(to_ = bob.address,
                                                      amount = 2,
                                                      token_id = 1)])
                ]).run(sender = op2, valid = False)
            scenario.p("Alice can remove their operator")
            scenario += c1.update_operators([
                sp.variant("remove_operator", Operator_param.make(
                    owner = alice.address,
                    operator = op1.address))
            ]).run(sender = alice)
            scenario.p("Operator1 cannot transfer Alice's tokens any more")
            scenario += c1.transfer(
                [
                    Batch_transfer.item(from_ = alice.address,
                                        txs = [
                                            sp.record(to_ = op1.address,
                                                      amount = 2,
                                                      token_id = 1)])
                ]).run(sender = op1, valid = False)
            scenario.p("Bob can add Operator0.")
            scenario += c1.update_operators([
                sp.variant("add_operator", Operator_param.make(
                    owner = bob.address,
                    operator = op0.address
                ))
            ]).run(sender = bob)
            scenario.p("Operator0 can transfer Bob's tokens '0' and '1'")
            scenario += c1.transfer(
                [
                    Batch_transfer.item(from_ = bob.address,
                                        txs = [
                                            sp.record(to_ = alice.address,
                                                      amount = 1,
                                                      token_id = 0)]),
                    Batch_transfer.item(from_ = bob.address,
                                        txs = [
                                            sp.record(to_ = alice.address,
                                                      amount = 1,
                                                      token_id = 1)])
                ]).run(sender = op0)
            scenario.p("Bob cannot add Operator0 for Alice's tokens.")
            scenario += c1.update_operators([
                sp.variant("add_operator", Operator_param.make(
                    owner = alice.address,
                    operator = op0.address
                ))
            ]).run(sender = bob, valid = False)
            scenario.p("Alice can also add Operator0 for themselves.")
            scenario += c1.update_operators([
                sp.variant("add_operator", Operator_param.make(
                    owner = alice.address,
                    operator = op0.address
                ))
            ]).run(sender = alice, valid = True)
            scenario.p("Operator0 can now transfer Bob's and Alice's tokens.")
            scenario += c1.transfer(
                [
                    Batch_transfer.item(from_ = bob.address,
                                        txs = [
                                            sp.record(to_ = alice.address,
                                                      amount = 1,
                                                      token_id = 0)]),
                    Batch_transfer.item(from_ = alice.address,
                                        txs = [
                                            sp.record(to_ = bob.address,
                                                      amount = 1,
                                                      token_id = 1)])
                ]).run(sender = op0)
            scenario.p("Bob adds Operator2 as second operator.")
            scenario += c1.update_operators([
                sp.variant("add_operator", Operator_param.make(
                    owner = bob.address,
                    operator = op2.address
                ))
            ]).run(sender = bob, valid = True)
            scenario.p("Operator0 and Operator2 can transfer Bob's tokens.")
            scenario += c1.transfer(
                [
                    Batch_transfer.item(from_ = bob.address,
                                        txs = [
                                            sp.record(to_ = alice.address,
                                                      amount = 1,
                                                      token_id = 0)])
                ]).run(sender = op0)
            scenario += c1.transfer(
                [
                    Batch_transfer.item(from_ = bob.address,
                                        txs = [
                                            sp.record(to_ = alice.address,
                                                      amount = 1,
                                                      token_id = 0)])
                ]).run(sender = op2)
            scenario.h3("Testing is_operator")
            scenario.p("Operator0 and Operator2 are still active for Bob's, Alice only has Operator0")
            def test_is_operator(scenario, owner, operator, result, comment):
                scenario.p("test_is_operator: " + comment )
                is_operator = Operator_param.make(
                    owner = owner.address,
                    operator = operator.address) #, tokens = tokens)
                scenario += c1.is_operator(
                    sp.record(
                        callback = sp.contract(
                            Operator_param.is_operator_response_type(),
                            sp.contract_address(consumer),
                            entry_point = "receive_is_operator").open_some(),
                        operator = is_operator
                    ))
                scenario.verify(consumer.data.last_operator == result)
                test_is_operator(scenario,   bob, op0,  True, "op0 for bob")
                test_is_operator(scenario,   bob, op1, False, "op1 for bob")
                test_is_operator(scenario,   bob, op2,  True, "op2 for bob")
                test_is_operator(scenario, alice, op0,  True, "op0 for alice")
                test_is_operator(scenario, alice, op1, False, "op1 for alice")
                test_is_operator(scenario, alice, op2, False, "op2 for alice")
                scenario.h3("Testing permissions_descriptor")
                scenario.verify(consumer.data.operator_support == False)
                scenario += c1.permissions_descriptor(
                    sp.contract(
                        Permissions_descriptor.get_type(),
                        sp.contract_address(consumer),
                        entry_point = "receive_permissions_descriptor").open_some())
                scenario.verify(consumer.data.operator_support == True)
