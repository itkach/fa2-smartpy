#! /bin/sh

set -e
usage () {
    cat >&2 <<EOF

EOF
}

check () {
    commands="opam SmartPy.sh tezos-client flextesa"
    for c in $commands ; do
        command -v $c >/dev/null || {
            echo "Error missing command: $c"
            echo "the project needs in \$PATH: $commands"
            exit 2
        }
    done
}

prepare () {
    check
    if ! [ -d _opam/ ] ; then
        opam switch create . 4.09.1
    fi
    eval $(opam env)
    opam install -y base fmt uri cmdliner ezjsonm \
         ocamlformat uri merlin ppx_deriving angstrom
}
eval $(opam env)

build_default () {
    # We want some error messages earlier:
    SmartPy.sh compile multi_asset.py \
               'FA2(sp.address("tz1asCc9HRuVkSdkw5eYF7vWiefpd5hCCWdG"))' \
               $PWD/_test_output
    SmartPy.sh test multi_asset.py  $PWD/_test_output
    sh src/app/gen_dune.sh > src/app/dune.inc
    #dune build @autogen --auto-promote
    dune build src/app/main.exe
    dune build @src/app/contracts
    ln -fs _build/default/src/app/main.exe fatoo
    echo "## Build Successful"
    find _build/default/src/app/ -name '*.tz'
}
build_tutorial () {
    output=${1:-_build/tutorial}
    pid_file=$output/sandbox.pid
    mkdir -p "$output"
    export alice="$(flextesa key alice)"
    export bob="$(flextesa key bob)"
    if [ "$no_sandbox" != "true" ] ; then
        flextesa mini-net \
                 --size 2 --time-b 2 --number-of-boot 2 --until 20_000_000 \
                 --add-bootstrap-account="$alice@2_000_000_000_000" \
                 --add-bootstrap-account="$bob@2_000_000_000_000" \
                 > "$output/sandbox.log" 2>&1 &
        echo $! > $pid_file
        echo "SANDBOX: $pid_file -> $(cat $pid_file), sleeping for a bit..."
        sleep 4
    fi
    tezos-client config reset
    tezos-client -P 20000 config update
    tezos-client bootstrapped
    for i in $(seq 1 10) ; do
        tezos-client rpc get /chains/main/blocks/1/protocols && break || {
                echo "Sandbox not ready"
                ps $(cat "$pid_file")
                sleep 3
            }
    done
    fatoo show-tuto -o "$output/fa2-tutorial.md" $tutorial_extra_options
    if [ "$no_sandbox" != "true" ] ; then
        echo "Killing sandbox"
        kill "$(cat $pid_file)"
    fi
    echo "See: $output/fa2-tutorial.md"
}

make_css () {
    cat > "$1" <<EOF
body {font-family: sans;}
pre {
  display:block;
  padding: 4px;
  overflow: auto;
  background-color: #eee;
  border: solid #aaa 1px;
}
blockquote {border-left: solid 2px #aaa; padding-left: 1em}
EOF
}

pandocize () {
    echo "Making $2"
    pandoc -s --toc -i "$1" -o "$2" --css ./style.css
}

build_website () {
    output=${1:-_build/website}
    tutorial=${2:-_build/tutorial/fa2-tutorial.md}
    mkdir -p "$output"
    make_css "$output/style.css"
    pandocize "$tutorial" "$output/tutorial.html"
    pandocize "README.md" "$output/implementation.html"
    cat > /tmp/index.md <<EOF
---
title: FA2-SmartPy — Home
---

See:

* [Tutorial](./tutorial.html)
* [Implementation entry-point](./implementation.html)
EOF
    pandocize /tmp/index.md "$output/index.html"
    echo "Done: file://$PWD/$output/index.html"
}

build () {
    case "$1" in
        "" ) build_default ;;
        * )
            c="$1"
            shift
            "build_$c" "$@" ;;
    esac
}

install () {
    bindir=${PREFIX:-/usr}/bin
    mkdir -p $bindir
    cp -f _build/default/src/app/main.exe $bindir/fatoo
    chmod a+rx $bindir/fatoo
}


run () {
    case "$1" in
        "simulations" )
            dune build @src/app/simulations
            find _build/default/src/app/ -name '*simulation-log.txt'
            ;;
        * )
            dune exec src/app/main.exe -- "$@" ;;
    esac
}

update_michelsons () {
    datetag="$(date -u +%Y%m%d-%H%M%S%z)"
    gittag="$(git describe --always --dirty)"
    dirpath="michelson/"
    path_prefix="$dirpath/${datetag}_${gittag}"
    mkdir -p "$dirpath"
    for c in contract dbg_contract mutran_contract noops_contract ; do
        path="${path_prefix}_$c.tz"
        echo "Making '$path'"
        run get-code "$c" -o "$path"
    done
}
update () {
    update_"$1" "$@"
}

lint () {
    find src \( -name "*.ml" -o -name "*.mli" \) -exec ocamlformat -i {} \;
}

if [ "$1" = "" ]; then
    build
else
    "$@"
fi
