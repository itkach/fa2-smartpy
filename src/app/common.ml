open! Base

module Console = struct
  let color_option =
    let default = Unix.isatty Unix.stdout in
    match Caml.Sys.getenv_opt "color" with
    | None -> default
    | Some "force" -> true
    | Some "none" -> false
    | Some _ -> false

  let output_formatter =
    let fmt = Fmt.stderr in
    Fmt.set_style_renderer fmt (if color_option then `Ansi_tty else `None) ;
    fmt

  let with_prompt prompt_fmt unit_fmt =
    let open Fmt in
    pf output_formatter "%a\n%!"
      (box ~indent:2 (prompt_fmt ++ sp ++ unit_fmt))
      ()

  let prompt ppf s = Fmt.pf ppf "[FA2->%s]:" s

  let say unit_fmt =
    with_prompt Fmt.(const (styled (`Fg (`Hi `Blue)) prompt) "Info") unit_fmt

  let warn unit_fmt =
    with_prompt
      Fmt.(const (styled (`Fg (`Hi `Yellow)) prompt) "Warning")
      unit_fmt

  let warnf fmt = Fmt.kstr (fun s -> warn Fmt.(const text s)) fmt

  let error unit_fmt =
    with_prompt Fmt.(const (styled (`Fg (`Hi `Red)) prompt) "ERROR") unit_fmt

  let errorf fmt = Fmt.kstr (fun s -> error Fmt.(const text s)) fmt
end

let debug = ref false

let () =
  debug :=
    match Caml.Sys.getenv "fa2_debug" with
    | "true" -> true
    | _ -> false
    | exception _ -> false

let dbg unit_fmt =
  if !debug then
    Console.with_prompt
      Fmt.(const (styled `Faint Console.prompt) "DEBUG")
      Fmt.(styled `Faint unit_fmt)

let dbgf fmt = Fmt.kstr (fun s -> dbg Fmt.(const string s)) fmt

module Io = struct
  include Result

  let fail_system ?(details = []) s =
    fail (`System ((s : string), (details : unit Fmt.t list)))

  let catch ?details f =
    try return (f ()) with e -> fail_system ?details (Exn.to_string e)

  let dispatch x ~ok ~error = match x with Ok x -> ok x | Error e -> error e

  let run f : [`Never_returns] =
    f ()
    |> function
    | Ok () -> Stdlib.exit 0
    | Error (`Of_json (s, json)) ->
        Console.error
          Fmt.(
            vbox ~indent:0
              ( box ~indent:2
                  ( const string "JSON-parsing:"
                  ++ sp ++ const text s ++ const string ":" )
              ++ cut
              ++ const lines (Ezjsonm.value_to_string ~minify:false json) )) ;
        Stdlib.exit 2
    | Error (`System (s, details)) ->
        let msg = Fmt.str "%s" s in
        Console.error
          Fmt.(
            vbox ~indent:0
              ( box ~indent:2 (const text msg)
              ++ const
                   (fun ppf -> function [] -> ()
                     | more ->
                         pf ppf "@,%a"
                           (vbox (list ~sep:cut (fun ppf f -> f ppf ())))
                           more)
                   details )) ;
        Stdlib.exit 3
end

module Path = Caml.Filename

let ( // ) = Path.concat

module State = struct
  type t = {root_path: string [@main]; mutable command_count: int [@default 0]}
  [@@deriving make, show, eq]

  let cmdliner_term () =
    let open Cmdliner in
    let open Term in
    const (fun p -> make p)
    $ Arg.(
        let env =
          env_var "fatoo_root_path"
            ~doc:
              "Override the default root-path (when option --root-path is not \
               provided)" in
        value
          (opt string "/tmp/fa2test"
             (info ["root-path"] ~docv:"PATH" ~env
                ~doc:"Set the root path for the whole experiment.")))

  let root_path t = t.root_path
  let client_base_dir t = root_path t // "client-base-dir"
end

module Command = struct
  module Command_result = struct
    open State

    type t = {id: int [@main]} [@@deriving make, show, eq]

    let fresh s =
      let r = make s.command_count in
      s.command_count <- s.command_count + 1 ;
      r

    let pp ppf i = Fmt.pf ppf "{command:%d}" i.id

    let output_path state {id} =
      let s =
        let b = Buffer.create 42 in
        Fmt.str "%08d" id |> String.to_list
        |> List.iteri ~f:(fun i c ->
               Buffer.add_char b c ;
               if i % 2 = 1 then Buffer.add_char b '/' else ()) ;
        Buffer.contents b in
      state.root_path // "commands" // s

    let get_stdout state i =
      Stdio.In_channel.read_all (output_path state i // "out.txt")

    let get_stderr state i =
      Stdio.In_channel.read_all (output_path state i // "err.txt")
  end

  let run ?(succeed = true) state command_string =
    let id = Command_result.fresh state in
    let outpath = Command_result.output_path state id in
    let decorated =
      let outq = Path.quote outpath in
      Fmt.str "mkdir -p %s && {  %s > %s/out.txt 2> %s/err.txt ; }" outq
        command_string outq outq in
    let ret = Caml.Sys.command decorated in
    Stdio.Out_channel.write_all (outpath // "cmd.txt") ~data:decorated ;
    (* dbg "cmd: %s → %d" decorated ret; *)
    if succeed && ret <> 0 then
      let details =
        Fmt.(
          List.map
            [ ("Command:", const string command_string)
            ; ("Stdout:", const string (outpath // "out.txt"))
            ; ("Stderr:", const string (outpath // "err.txt"))
            ; ( "Error"
              , fun ppf () ->
                  let extract =
                    Stdio.In_channel.read_lines (outpath // "err.txt")
                    |> List.rev
                    |> (fun l -> List.take l 10)
                    |> List.rev in
                  (vbox (list ~sep:cut (const string "| " ++ string)))
                    ppf extract ) ]
            ~f:(fun (k, v) ->
              box ~indent:2 (const string "* " ++ const string k ++ sp ++ v)))
      in
      Fmt.kstr (Io.fail_system ~details) "Command %s (%a) failed" command_string
        Command_result.pp id
    else Io.return id
end
