module Switches = struct
  type t =
    { debug: bool
    ; carthage_pairs: bool
    ; force_layouts: bool
    ; support_operator: bool
    ; assume_consecutive_token_ids: bool
    ; add_mutez_transfer: bool
    ; single_asset: bool }
  [@@deriving show, eq]
end

module type CONTRACT = sig
  val name : string
  val metadata_string : string
  val description : string
  val switches : Switches.t
  val code : string
end

module type CONTRACT_INTERFACE = sig
  module Json_value : sig
    type t =
      [ `A of t list
      | `Bool of bool
      | `Float of float
      | `Null
      | `O of (string * t) list
      | `String of string ]

    val pp :
      Ppx_deriving_runtime.Format.formatter -> t -> Ppx_deriving_runtime.unit

    val show : t -> Ppx_deriving_runtime.string
    val equal : t -> t -> Ppx_deriving_runtime.bool

    type parse_error = [`Of_json of string * t]

    val pp_parse_error :
         Ppx_deriving_runtime.Format.formatter
      -> parse_error
      -> Ppx_deriving_runtime.unit

    val show_parse_error : parse_error -> Ppx_deriving_runtime.string

    val equal_parse_error :
      parse_error -> parse_error -> Ppx_deriving_runtime.bool
  end

  module M_nat : sig
    type t = Int of int

    val pp :
      Ppx_deriving_runtime.Format.formatter -> t -> Ppx_deriving_runtime.unit

    val show : t -> Ppx_deriving_runtime.string
    val equal : t -> t -> Ppx_deriving_runtime.bool
    val to_concrete : t -> string
    val of_json : Json_value.t -> (t, [> Json_value.parse_error]) result
  end

  module M_address : sig
    type t = Raw_b58 of string | Raw_hex_bytes of string

    val pp :
      Ppx_deriving_runtime.Format.formatter -> t -> Ppx_deriving_runtime.unit

    val show : t -> Ppx_deriving_runtime.string
    val equal : t -> t -> Ppx_deriving_runtime.bool
    val to_concrete : t -> string
    val of_json : Json_value.t -> (t, [> Json_value.parse_error]) result
  end

  module M_string : sig
    type t = Raw_string of string | Raw_hex_bytes of string

    val pp :
      Ppx_deriving_runtime.Format.formatter -> t -> Ppx_deriving_runtime.unit

    val show : t -> Ppx_deriving_runtime.string
    val equal : t -> t -> Ppx_deriving_runtime.bool
    val to_concrete : t -> string
    val of_json : Json_value.t -> (t, [> Json_value.parse_error]) result
  end

  module M_list : sig
    type 'a t = 'a list [@@deriving show, eq]

    val to_concrete : ('a -> string) -> 'a t -> string

    val of_json :
         (Json_value.t -> ('elt, ([> Json_value.parse_error] as 'a)) result)
      -> Json_value.t
      -> ('elt t, 'a) result
  end

  module Txs_element : sig
    type t [@@deriving show, eq]

    val make : amount:M_nat.t -> to_:M_address.t -> token_id:M_nat.t -> t
    val to_concrete : t -> string
  end

  module Transfer_element : sig
    type t [@@deriving show, eq]

    val make : from_:M_address.t -> txs:Txs_element.t list -> t
    val to_concrete : t -> string
  end

  module Mint : sig
    type t [@@deriving show, eq]

    val make :
         address:M_address.t
      -> amount:M_nat.t
      -> symbol:M_string.t
      -> token_id:M_nat.t
      -> t

    val to_concrete : t -> string
  end

  (*
  module M_set : sig
    type 'a t = 'a list [@@deriving show, eq]

    val to_concrete : ('a -> string) -> 'a t -> string

    val of_json :
         (Json_value.t -> ('elt, ([> Json_value.parse_error] as 'a)) result)
      -> Json_value.t
      -> ('elt t, 'a) result
  end
*)

  module M_unit : sig
    type t = Unit [@@deriving show, eq]

    val to_concrete : t -> string
  end

  (*
module Tokens : sig
    type t = All_tokens of M_unit.t | Some_tokens of M_nat.t M_set.t
    [@@deriving show, eq]

    val to_concrete : t -> string
  end
 *)

  module Remove_operator : sig
    type t [@@deriving show, eq]

    val make :
      operator:M_address.t -> owner:M_address.t (*  -> tokens:Tokens.t *) -> t
  end

  module Add_operator : sig
    type t [@@deriving show, eq]

    val make :
      operator:M_address.t -> owner:M_address.t (*  -> tokens:Tokens.t *) -> t
  end

  module Update_operators_element : sig
    type t =
      | Add_operator of Add_operator.t
      | Remove_operator of Remove_operator.t
    [@@deriving show, eq]

    val to_concrete : t -> string
    val to_concrete_entry_point : t -> [`Name of string] * [`Literal of string]
    val of_json : Json_value.t -> (t, [> Json_value.parse_error]) result
  end

  module Storage : sig
    type t [@@deriving show, eq]

    val to_concrete : t -> string
    val of_json : Json_value.t -> (t, [> Json_value.parse_error]) result
  end

  val storage_of_address : string -> Storage.t
end
