;; Name: contract
;; dbg    : false
;; capa   : true
;; layout : true
;; ops    : true
;; toknat : true
;; mutran : false
;; single : false

(rule
  (target fa2_code_contract.ml)
  (deps fa2_contract.tz)
  (action
    (with-stdout-to %{target}
      (progn
       (echo "let code = {letshopenobodyusesthisstring|")
       (run cat fa2_contract.tz)
       (echo "|letshopenobodyusesthisstring}")
))))

(rule
  (target fa2_contract_contract.ml)
  (action
    (with-stdout-to %{target}
     (progn
       (echo "let name = \"contract\"\n")
       (echo "let metadata_string = \"version_20200601_tzip_b916f32_contract\"\n")
       (echo "let description = \"The default\"\n")
       (echo "let switches = Sigs.Switches.{\n")
       (echo "debug = false\n")
       (echo "; carthage_pairs = true\n")
       (echo "; force_layouts = true\n")
       (echo "; support_operator = true\n")
       (echo "; assume_consecutive_token_ids = true\n")
       (echo "; add_mutez_transfer = false\n")
       (echo "; single_asset = false\n")
       (echo "}\n")
       (echo "let code = Fa2_code_contract.code\n")
     )
)))

(rule
  (targets fa2_contract.tz _fa2_intermediate_contract.tz)
  (deps multi_asset.py fa2_contract_contract.ml)
  (action
(setenv debug_mode "false"
 (setenv carthage_pairs "true"
  (setenv force_layouts "true"
   (setenv support_operator "true"
    (setenv assume_consecutive_token_ids "true"
     (setenv add_mutez_transfer "false"
       (setenv single_asset "false"
      (progn
        (run SmartPy.sh compile-smartpy-class multi_asset.py
             "FA2(sp.address(\"tz1L1bypLzuxGHmx3d6bHFJ2WCi4ZDbocSCA\"))"
             _fa2_contract)
        (run cp  _fa2_contract/multi_asset_compiled.tz _fa2_intermediate_contract.tz)
        (run flextesa transform
             --replace-annot %self_:%self,%metadata_string:%version_20200601_tzip_b916f32_contract
             _fa2_contract/multi_asset_compiled.tz fa2_contract.tz)
      )
)))))))
))

(rule
   (target fa2_tmp_contract.ml)
   (deps fa2_contract.tz)
   (action (progn
     (run flextesa ocaml --integer-types "" %{deps} %{target})
)))
(rule
   (target fa2_interface_contract.ml)
   (deps fa2_tmp_contract.ml)
   (action
     (with-stdout-to %{target}
       (progn
        (cat fa2_tmp_contract.ml)
        (echo "\n\nlet storage_of_address addr = Storage.make                ~version_20200601_tzip_b916f32_contract:M_unit.Unit                ~administrator:M_address.(Raw_b58 addr)                ~all_tokens:M_nat.(Int 0)                ~ledger:M_big_map.(List []) ~paused:M_bool.False                 ~operators:M_big_map.(List [])                 ~tokens:M_big_map.(List [])\n\n")
       )
     )
   )
)

(rule
  (target fa2_contract-simulation-log.txt)
  (deps multi_asset.py fa2_contract_contract.ml)
  (action
(setenv debug_mode "false"
 (setenv carthage_pairs "true"
  (setenv force_layouts "true"
   (setenv support_operator "true"
    (setenv assume_consecutive_token_ids "true"
     (setenv add_mutez_transfer "false"
       (setenv single_asset "false"
    (progn
       (with-accepted-exit-codes (or 0 1) ;; We allow failures for now
         (run SmartPy.sh test multi_asset.py _fa2_simulation_contract))
       (with-stdout-to %{target} (echo "Test contract"))
       (with-accepted-exit-codes (or 0 1) ;; We allow failures for now
         (run mv
         _fa2_simulation_contract/interpreted-scenario/scenario-interpreter-log.txt
          %{target} ))
    )
)))))))
))
;; Name: dbg_contract
;; dbg    : true
;; capa   : true
;; layout : true
;; ops    : true
;; toknat : true
;; mutran : false
;; single : false

(rule
  (target fa2_code_dbg_contract.ml)
  (deps fa2_dbg_contract.tz)
  (action
    (with-stdout-to %{target}
      (progn
       (echo "let code = {letshopenobodyusesthisstring|")
       (run cat fa2_dbg_contract.tz)
       (echo "|letshopenobodyusesthisstring}")
))))

(rule
  (target fa2_contract_dbg_contract.ml)
  (action
    (with-stdout-to %{target}
     (progn
       (echo "let name = \"dbg_contract\"\n")
       (echo "let metadata_string = \"version_20200601_tzip_b916f32_dbg_contract\"\n")
       (echo "let description = \"The default in debug mode\"\n")
       (echo "let switches = Sigs.Switches.{\n")
       (echo "debug = true\n")
       (echo "; carthage_pairs = true\n")
       (echo "; force_layouts = true\n")
       (echo "; support_operator = true\n")
       (echo "; assume_consecutive_token_ids = true\n")
       (echo "; add_mutez_transfer = false\n")
       (echo "; single_asset = false\n")
       (echo "}\n")
       (echo "let code = Fa2_code_dbg_contract.code\n")
     )
)))

(rule
  (targets fa2_dbg_contract.tz _fa2_intermediate_dbg_contract.tz)
  (deps multi_asset.py fa2_contract_dbg_contract.ml)
  (action
(setenv debug_mode "true"
 (setenv carthage_pairs "true"
  (setenv force_layouts "true"
   (setenv support_operator "true"
    (setenv assume_consecutive_token_ids "true"
     (setenv add_mutez_transfer "false"
       (setenv single_asset "false"
      (progn
        (run SmartPy.sh compile-smartpy-class multi_asset.py
             "FA2(sp.address(\"tz1L1bypLzuxGHmx3d6bHFJ2WCi4ZDbocSCA\"))"
             _fa2_dbg_contract)
        (run cp  _fa2_dbg_contract/multi_asset_compiled.tz _fa2_intermediate_dbg_contract.tz)
        (run flextesa transform
             --replace-annot %self_:%self,%metadata_string:%version_20200601_tzip_b916f32_dbg_contract
             _fa2_dbg_contract/multi_asset_compiled.tz fa2_dbg_contract.tz)
      )
)))))))
))

(rule
   (target fa2_tmp_dbg_contract.ml)
   (deps fa2_dbg_contract.tz)
   (action (progn
     (run flextesa ocaml --integer-types "" %{deps} %{target})
)))
(rule
   (target fa2_interface_dbg_contract.ml)
   (deps fa2_tmp_dbg_contract.ml)
   (action
     (with-stdout-to %{target}
       (progn
        (cat fa2_tmp_dbg_contract.ml)
        (echo "\n\nlet storage_of_address addr = Storage.make                ~version_20200601_tzip_b916f32_dbg_contract:M_unit.Unit                ~administrator:M_address.(Raw_b58 addr)                ~all_tokens:M_nat.(Int 0)                ~ledger:[] ~paused:M_bool.False                 ~operators:[]                 ~tokens:[]\n\n")
       )
     )
   )
)

(rule
  (target fa2_dbg_contract-simulation-log.txt)
  (deps multi_asset.py fa2_contract_dbg_contract.ml)
  (action
(setenv debug_mode "true"
 (setenv carthage_pairs "true"
  (setenv force_layouts "true"
   (setenv support_operator "true"
    (setenv assume_consecutive_token_ids "true"
     (setenv add_mutez_transfer "false"
       (setenv single_asset "false"
    (progn
       (with-accepted-exit-codes (or 0 1) ;; We allow failures for now
         (run SmartPy.sh test multi_asset.py _fa2_simulation_dbg_contract))
       (with-stdout-to %{target} (echo "Test dbg_contract"))
       (with-accepted-exit-codes (or 0 1) ;; We allow failures for now
         (run mv
         _fa2_simulation_dbg_contract/interpreted-scenario/scenario-interpreter-log.txt
          %{target} ))
    )
)))))))
))
;; Name: baby_contract
;; dbg    : false
;; capa   : false
;; layout : true
;; ops    : true
;; toknat : true
;; mutran : false
;; single : false

(rule
  (target fa2_code_baby_contract.ml)
  (deps fa2_baby_contract.tz)
  (action
    (with-stdout-to %{target}
      (progn
       (echo "let code = {letshopenobodyusesthisstring|")
       (run cat fa2_baby_contract.tz)
       (echo "|letshopenobodyusesthisstring}")
))))

(rule
  (target fa2_contract_baby_contract.ml)
  (action
    (with-stdout-to %{target}
     (progn
       (echo "let name = \"baby_contract\"\n")
       (echo "let metadata_string = \"version_20200601_tzip_b916f32_baby_contract\"\n")
       (echo "let description = \"The default in Babylon mode\"\n")
       (echo "let switches = Sigs.Switches.{\n")
       (echo "debug = false\n")
       (echo "; carthage_pairs = false\n")
       (echo "; force_layouts = true\n")
       (echo "; support_operator = true\n")
       (echo "; assume_consecutive_token_ids = true\n")
       (echo "; add_mutez_transfer = false\n")
       (echo "; single_asset = false\n")
       (echo "}\n")
       (echo "let code = Fa2_code_baby_contract.code\n")
     )
)))

(rule
  (targets fa2_baby_contract.tz _fa2_intermediate_baby_contract.tz)
  (deps multi_asset.py fa2_contract_baby_contract.ml)
  (action
(setenv debug_mode "false"
 (setenv carthage_pairs "false"
  (setenv force_layouts "true"
   (setenv support_operator "true"
    (setenv assume_consecutive_token_ids "true"
     (setenv add_mutez_transfer "false"
       (setenv single_asset "false"
      (progn
        (run SmartPy.sh compile-smartpy-class multi_asset.py
             "FA2(sp.address(\"tz1L1bypLzuxGHmx3d6bHFJ2WCi4ZDbocSCA\"))"
             _fa2_baby_contract)
        (run cp  _fa2_baby_contract/multi_asset_compiled.tz _fa2_intermediate_baby_contract.tz)
        (run flextesa transform
             --replace-annot %self_:%self,%metadata_string:%version_20200601_tzip_b916f32_baby_contract
             _fa2_baby_contract/multi_asset_compiled.tz fa2_baby_contract.tz)
      )
)))))))
))

(rule
   (target fa2_tmp_baby_contract.ml)
   (deps fa2_baby_contract.tz)
   (action (progn
     (run flextesa ocaml --integer-types "" %{deps} %{target})
)))
(rule
   (target fa2_interface_baby_contract.ml)
   (deps fa2_tmp_baby_contract.ml)
   (action
     (with-stdout-to %{target}
       (progn
        (cat fa2_tmp_baby_contract.ml)
        (echo "\n\nlet storage_of_address addr = Storage.make                ~version_20200601_tzip_b916f32_baby_contract:M_unit.Unit                ~administrator:M_address.(Raw_b58 addr)                ~all_tokens:M_nat.(Int 0)                ~ledger:M_big_map.(List []) ~paused:M_bool.False                 ~operators:M_big_map.(List [])                 ~tokens:M_big_map.(List [])\n\n")
       )
     )
   )
)

(rule
  (target fa2_baby_contract-simulation-log.txt)
  (deps multi_asset.py fa2_contract_baby_contract.ml)
  (action
(setenv debug_mode "false"
 (setenv carthage_pairs "false"
  (setenv force_layouts "true"
   (setenv support_operator "true"
    (setenv assume_consecutive_token_ids "true"
     (setenv add_mutez_transfer "false"
       (setenv single_asset "false"
    (progn
       (with-accepted-exit-codes (or 0 1) ;; We allow failures for now
         (run SmartPy.sh test multi_asset.py _fa2_simulation_baby_contract))
       (with-stdout-to %{target} (echo "Test baby_contract"))
       (with-accepted-exit-codes (or 0 1) ;; We allow failures for now
         (run mv
         _fa2_simulation_baby_contract/interpreted-scenario/scenario-interpreter-log.txt
          %{target} ))
    )
)))))))
))
;; Name: nolay_contract
;; dbg    : false
;; capa   : true
;; layout : false
;; ops    : true
;; toknat : true
;; mutran : false
;; single : false

(rule
  (target fa2_code_nolay_contract.ml)
  (deps fa2_nolay_contract.tz)
  (action
    (with-stdout-to %{target}
      (progn
       (echo "let code = {letshopenobodyusesthisstring|")
       (run cat fa2_nolay_contract.tz)
       (echo "|letshopenobodyusesthisstring}")
))))

(rule
  (target fa2_contract_nolay_contract.ml)
  (action
    (with-stdout-to %{target}
     (progn
       (echo "let name = \"nolay_contract\"\n")
       (echo "let metadata_string = \"version_20200601_tzip_b916f32_nolay_contract\"\n")
       (echo "let description = \"The default without right-combs\"\n")
       (echo "let switches = Sigs.Switches.{\n")
       (echo "debug = false\n")
       (echo "; carthage_pairs = true\n")
       (echo "; force_layouts = false\n")
       (echo "; support_operator = true\n")
       (echo "; assume_consecutive_token_ids = true\n")
       (echo "; add_mutez_transfer = false\n")
       (echo "; single_asset = false\n")
       (echo "}\n")
       (echo "let code = Fa2_code_nolay_contract.code\n")
     )
)))

(rule
  (targets fa2_nolay_contract.tz _fa2_intermediate_nolay_contract.tz)
  (deps multi_asset.py fa2_contract_nolay_contract.ml)
  (action
(setenv debug_mode "false"
 (setenv carthage_pairs "true"
  (setenv force_layouts "false"
   (setenv support_operator "true"
    (setenv assume_consecutive_token_ids "true"
     (setenv add_mutez_transfer "false"
       (setenv single_asset "false"
      (progn
        (run SmartPy.sh compile-smartpy-class multi_asset.py
             "FA2(sp.address(\"tz1L1bypLzuxGHmx3d6bHFJ2WCi4ZDbocSCA\"))"
             _fa2_nolay_contract)
        (run cp  _fa2_nolay_contract/multi_asset_compiled.tz _fa2_intermediate_nolay_contract.tz)
        (run flextesa transform
             --replace-annot %self_:%self,%metadata_string:%version_20200601_tzip_b916f32_nolay_contract
             _fa2_nolay_contract/multi_asset_compiled.tz fa2_nolay_contract.tz)
      )
)))))))
))

(rule
   (target fa2_tmp_nolay_contract.ml)
   (deps fa2_nolay_contract.tz)
   (action (progn
     (run flextesa ocaml --integer-types "" %{deps} %{target})
)))
(rule
   (target fa2_interface_nolay_contract.ml)
   (deps fa2_tmp_nolay_contract.ml)
   (action
     (with-stdout-to %{target}
       (progn
        (cat fa2_tmp_nolay_contract.ml)
        (echo "\n\nlet storage_of_address addr = Storage.make                ~version_20200601_tzip_b916f32_nolay_contract:M_unit.Unit                ~administrator:M_address.(Raw_b58 addr)                ~all_tokens:M_nat.(Int 0)                ~ledger:M_big_map.(List []) ~paused:M_bool.False                 ~operators:M_big_map.(List [])                 ~tokens:M_big_map.(List [])\n\n")
       )
     )
   )
)

(rule
  (target fa2_nolay_contract-simulation-log.txt)
  (deps multi_asset.py fa2_contract_nolay_contract.ml)
  (action
(setenv debug_mode "false"
 (setenv carthage_pairs "true"
  (setenv force_layouts "false"
   (setenv support_operator "true"
    (setenv assume_consecutive_token_ids "true"
     (setenv add_mutez_transfer "false"
       (setenv single_asset "false"
    (progn
       (with-accepted-exit-codes (or 0 1) ;; We allow failures for now
         (run SmartPy.sh test multi_asset.py _fa2_simulation_nolay_contract))
       (with-stdout-to %{target} (echo "Test nolay_contract"))
       (with-accepted-exit-codes (or 0 1) ;; We allow failures for now
         (run mv
         _fa2_simulation_nolay_contract/interpreted-scenario/scenario-interpreter-log.txt
          %{target} ))
    )
)))))))
))
;; Name: mutran_contract
;; dbg    : false
;; capa   : true
;; layout : true
;; ops    : true
;; toknat : true
;; mutran : true
;; single : false

(rule
  (target fa2_code_mutran_contract.ml)
  (deps fa2_mutran_contract.tz)
  (action
    (with-stdout-to %{target}
      (progn
       (echo "let code = {letshopenobodyusesthisstring|")
       (run cat fa2_mutran_contract.tz)
       (echo "|letshopenobodyusesthisstring}")
))))

(rule
  (target fa2_contract_mutran_contract.ml)
  (action
    (with-stdout-to %{target}
     (progn
       (echo "let name = \"mutran_contract\"\n")
       (echo "let metadata_string = \"version_20200601_tzip_b916f32_mutran_contract\"\n")
       (echo "let description = \"The default with mutez transfer entry-point\"\n")
       (echo "let switches = Sigs.Switches.{\n")
       (echo "debug = false\n")
       (echo "; carthage_pairs = true\n")
       (echo "; force_layouts = true\n")
       (echo "; support_operator = true\n")
       (echo "; assume_consecutive_token_ids = true\n")
       (echo "; add_mutez_transfer = true\n")
       (echo "; single_asset = false\n")
       (echo "}\n")
       (echo "let code = Fa2_code_mutran_contract.code\n")
     )
)))

(rule
  (targets fa2_mutran_contract.tz _fa2_intermediate_mutran_contract.tz)
  (deps multi_asset.py fa2_contract_mutran_contract.ml)
  (action
(setenv debug_mode "false"
 (setenv carthage_pairs "true"
  (setenv force_layouts "true"
   (setenv support_operator "true"
    (setenv assume_consecutive_token_ids "true"
     (setenv add_mutez_transfer "true"
       (setenv single_asset "false"
      (progn
        (run SmartPy.sh compile-smartpy-class multi_asset.py
             "FA2(sp.address(\"tz1L1bypLzuxGHmx3d6bHFJ2WCi4ZDbocSCA\"))"
             _fa2_mutran_contract)
        (run cp  _fa2_mutran_contract/multi_asset_compiled.tz _fa2_intermediate_mutran_contract.tz)
        (run flextesa transform
             --replace-annot %self_:%self,%metadata_string:%version_20200601_tzip_b916f32_mutran_contract
             _fa2_mutran_contract/multi_asset_compiled.tz fa2_mutran_contract.tz)
      )
)))))))
))

(rule
   (target fa2_tmp_mutran_contract.ml)
   (deps fa2_mutran_contract.tz)
   (action (progn
     (run flextesa ocaml --integer-types "" %{deps} %{target})
)))
(rule
   (target fa2_interface_mutran_contract.ml)
   (deps fa2_tmp_mutran_contract.ml)
   (action
     (with-stdout-to %{target}
       (progn
        (cat fa2_tmp_mutran_contract.ml)
        (echo "\n\nlet storage_of_address addr = Storage.make                ~version_20200601_tzip_b916f32_mutran_contract:M_unit.Unit                ~administrator:M_address.(Raw_b58 addr)                ~all_tokens:M_nat.(Int 0)                ~ledger:M_big_map.(List []) ~paused:M_bool.False                 ~operators:M_big_map.(List [])                 ~tokens:M_big_map.(List [])\n\n")
       )
     )
   )
)

(rule
  (target fa2_mutran_contract-simulation-log.txt)
  (deps multi_asset.py fa2_contract_mutran_contract.ml)
  (action
(setenv debug_mode "false"
 (setenv carthage_pairs "true"
  (setenv force_layouts "true"
   (setenv support_operator "true"
    (setenv assume_consecutive_token_ids "true"
     (setenv add_mutez_transfer "true"
       (setenv single_asset "false"
    (progn
       (with-accepted-exit-codes (or 0 1) ;; We allow failures for now
         (run SmartPy.sh test multi_asset.py _fa2_simulation_mutran_contract))
       (with-stdout-to %{target} (echo "Test mutran_contract"))
       (with-accepted-exit-codes (or 0 1) ;; We allow failures for now
         (run mv
         _fa2_simulation_mutran_contract/interpreted-scenario/scenario-interpreter-log.txt
          %{target} ))
    )
)))))))
))
;; Name: tokset_contract
;; dbg    : false
;; capa   : true
;; layout : true
;; ops    : true
;; toknat : false
;; mutran : false
;; single : false

(rule
  (target fa2_code_tokset_contract.ml)
  (deps fa2_tokset_contract.tz)
  (action
    (with-stdout-to %{target}
      (progn
       (echo "let code = {letshopenobodyusesthisstring|")
       (run cat fa2_tokset_contract.tz)
       (echo "|letshopenobodyusesthisstring}")
))))

(rule
  (target fa2_contract_tokset_contract.ml)
  (action
    (with-stdout-to %{target}
     (progn
       (echo "let name = \"tokset_contract\"\n")
       (echo "let metadata_string = \"version_20200601_tzip_b916f32_tokset_contract\"\n")
       (echo "let description = \"The default with non-consecutive token-IDs\"\n")
       (echo "let switches = Sigs.Switches.{\n")
       (echo "debug = false\n")
       (echo "; carthage_pairs = true\n")
       (echo "; force_layouts = true\n")
       (echo "; support_operator = true\n")
       (echo "; assume_consecutive_token_ids = false\n")
       (echo "; add_mutez_transfer = false\n")
       (echo "; single_asset = false\n")
       (echo "}\n")
       (echo "let code = Fa2_code_tokset_contract.code\n")
     )
)))

(rule
  (targets fa2_tokset_contract.tz _fa2_intermediate_tokset_contract.tz)
  (deps multi_asset.py fa2_contract_tokset_contract.ml)
  (action
(setenv debug_mode "false"
 (setenv carthage_pairs "true"
  (setenv force_layouts "true"
   (setenv support_operator "true"
    (setenv assume_consecutive_token_ids "false"
     (setenv add_mutez_transfer "false"
       (setenv single_asset "false"
      (progn
        (run SmartPy.sh compile-smartpy-class multi_asset.py
             "FA2(sp.address(\"tz1L1bypLzuxGHmx3d6bHFJ2WCi4ZDbocSCA\"))"
             _fa2_tokset_contract)
        (run cp  _fa2_tokset_contract/multi_asset_compiled.tz _fa2_intermediate_tokset_contract.tz)
        (run flextesa transform
             --replace-annot %self_:%self,%metadata_string:%version_20200601_tzip_b916f32_tokset_contract
             _fa2_tokset_contract/multi_asset_compiled.tz fa2_tokset_contract.tz)
      )
)))))))
))

(rule
   (target fa2_tmp_tokset_contract.ml)
   (deps fa2_tokset_contract.tz)
   (action (progn
     (run flextesa ocaml --integer-types "" %{deps} %{target})
)))
(rule
   (target fa2_interface_tokset_contract.ml)
   (deps fa2_tmp_tokset_contract.ml)
   (action
     (with-stdout-to %{target}
       (progn
        (cat fa2_tmp_tokset_contract.ml)
        (echo "\n\nlet storage_of_address addr = Storage.make                ~version_20200601_tzip_b916f32_tokset_contract:M_unit.Unit                ~administrator:M_address.(Raw_b58 addr)                ~all_tokens:[]                ~ledger:M_big_map.(List []) ~paused:M_bool.False                 ~operators:M_big_map.(List [])                 ~tokens:M_big_map.(List [])\n\n")
       )
     )
   )
)

(rule
  (target fa2_tokset_contract-simulation-log.txt)
  (deps multi_asset.py fa2_contract_tokset_contract.ml)
  (action
(setenv debug_mode "false"
 (setenv carthage_pairs "true"
  (setenv force_layouts "true"
   (setenv support_operator "true"
    (setenv assume_consecutive_token_ids "false"
     (setenv add_mutez_transfer "false"
       (setenv single_asset "false"
    (progn
       (with-accepted-exit-codes (or 0 1) ;; We allow failures for now
         (run SmartPy.sh test multi_asset.py _fa2_simulation_tokset_contract))
       (with-stdout-to %{target} (echo "Test tokset_contract"))
       (with-accepted-exit-codes (or 0 1) ;; We allow failures for now
         (run mv
         _fa2_simulation_tokset_contract/interpreted-scenario/scenario-interpreter-log.txt
          %{target} ))
    )
)))))))
))
;; Name: noops_contract
;; dbg    : false
;; capa   : true
;; layout : true
;; ops    : false
;; toknat : true
;; mutran : false
;; single : false

(rule
  (target fa2_code_noops_contract.ml)
  (deps fa2_noops_contract.tz)
  (action
    (with-stdout-to %{target}
      (progn
       (echo "let code = {letshopenobodyusesthisstring|")
       (run cat fa2_noops_contract.tz)
       (echo "|letshopenobodyusesthisstring}")
))))

(rule
  (target fa2_contract_noops_contract.ml)
  (action
    (with-stdout-to %{target}
     (progn
       (echo "let name = \"noops_contract\"\n")
       (echo "let metadata_string = \"version_20200601_tzip_b916f32_noops_contract\"\n")
       (echo "let description = \"The default without operators\"\n")
       (echo "let switches = Sigs.Switches.{\n")
       (echo "debug = false\n")
       (echo "; carthage_pairs = true\n")
       (echo "; force_layouts = true\n")
       (echo "; support_operator = false\n")
       (echo "; assume_consecutive_token_ids = true\n")
       (echo "; add_mutez_transfer = false\n")
       (echo "; single_asset = false\n")
       (echo "}\n")
       (echo "let code = Fa2_code_noops_contract.code\n")
     )
)))

(rule
  (targets fa2_noops_contract.tz _fa2_intermediate_noops_contract.tz)
  (deps multi_asset.py fa2_contract_noops_contract.ml)
  (action
(setenv debug_mode "false"
 (setenv carthage_pairs "true"
  (setenv force_layouts "true"
   (setenv support_operator "false"
    (setenv assume_consecutive_token_ids "true"
     (setenv add_mutez_transfer "false"
       (setenv single_asset "false"
      (progn
        (run SmartPy.sh compile-smartpy-class multi_asset.py
             "FA2(sp.address(\"tz1L1bypLzuxGHmx3d6bHFJ2WCi4ZDbocSCA\"))"
             _fa2_noops_contract)
        (run cp  _fa2_noops_contract/multi_asset_compiled.tz _fa2_intermediate_noops_contract.tz)
        (run flextesa transform
             --replace-annot %self_:%self,%metadata_string:%version_20200601_tzip_b916f32_noops_contract
             _fa2_noops_contract/multi_asset_compiled.tz fa2_noops_contract.tz)
      )
)))))))
))

(rule
   (target fa2_tmp_noops_contract.ml)
   (deps fa2_noops_contract.tz)
   (action (progn
     (run flextesa ocaml --integer-types "" %{deps} %{target})
)))
(rule
   (target fa2_interface_noops_contract.ml)
   (deps fa2_tmp_noops_contract.ml)
   (action
     (with-stdout-to %{target}
       (progn
        (cat fa2_tmp_noops_contract.ml)
        (echo "\n\nlet storage_of_address addr = Storage.make                ~version_20200601_tzip_b916f32_noops_contract:M_unit.Unit                ~administrator:M_address.(Raw_b58 addr)                ~all_tokens:M_nat.(Int 0)                ~ledger:M_big_map.(List []) ~paused:M_bool.False                 ~operators:M_big_map.(List [])                 ~tokens:M_big_map.(List [])\n\n")
       )
     )
   )
)

(rule
  (target fa2_noops_contract-simulation-log.txt)
  (deps multi_asset.py fa2_contract_noops_contract.ml)
  (action
(setenv debug_mode "false"
 (setenv carthage_pairs "true"
  (setenv force_layouts "true"
   (setenv support_operator "false"
    (setenv assume_consecutive_token_ids "true"
     (setenv add_mutez_transfer "false"
       (setenv single_asset "false"
    (progn
       (with-accepted-exit-codes (or 0 1) ;; We allow failures for now
         (run SmartPy.sh test multi_asset.py _fa2_simulation_noops_contract))
       (with-stdout-to %{target} (echo "Test noops_contract"))
       (with-accepted-exit-codes (or 0 1) ;; We allow failures for now
         (run mv
         _fa2_simulation_noops_contract/interpreted-scenario/scenario-interpreter-log.txt
          %{target} ))
    )
)))))))
))
;; Name: noops_dbg_contract
;; dbg    : true
;; capa   : true
;; layout : true
;; ops    : false
;; toknat : true
;; mutran : false
;; single : false

(rule
  (target fa2_code_noops_dbg_contract.ml)
  (deps fa2_noops_dbg_contract.tz)
  (action
    (with-stdout-to %{target}
      (progn
       (echo "let code = {letshopenobodyusesthisstring|")
       (run cat fa2_noops_dbg_contract.tz)
       (echo "|letshopenobodyusesthisstring}")
))))

(rule
  (target fa2_contract_noops_dbg_contract.ml)
  (action
    (with-stdout-to %{target}
     (progn
       (echo "let name = \"noops_dbg_contract\"\n")
       (echo "let metadata_string = \"version_20200601_tzip_b916f32_noops_dbg_contract\"\n")
       (echo "let description = \"The default without operators in debug mode\"\n")
       (echo "let switches = Sigs.Switches.{\n")
       (echo "debug = true\n")
       (echo "; carthage_pairs = true\n")
       (echo "; force_layouts = true\n")
       (echo "; support_operator = false\n")
       (echo "; assume_consecutive_token_ids = true\n")
       (echo "; add_mutez_transfer = false\n")
       (echo "; single_asset = false\n")
       (echo "}\n")
       (echo "let code = Fa2_code_noops_dbg_contract.code\n")
     )
)))

(rule
  (targets fa2_noops_dbg_contract.tz _fa2_intermediate_noops_dbg_contract.tz)
  (deps multi_asset.py fa2_contract_noops_dbg_contract.ml)
  (action
(setenv debug_mode "true"
 (setenv carthage_pairs "true"
  (setenv force_layouts "true"
   (setenv support_operator "false"
    (setenv assume_consecutive_token_ids "true"
     (setenv add_mutez_transfer "false"
       (setenv single_asset "false"
      (progn
        (run SmartPy.sh compile-smartpy-class multi_asset.py
             "FA2(sp.address(\"tz1L1bypLzuxGHmx3d6bHFJ2WCi4ZDbocSCA\"))"
             _fa2_noops_dbg_contract)
        (run cp  _fa2_noops_dbg_contract/multi_asset_compiled.tz _fa2_intermediate_noops_dbg_contract.tz)
        (run flextesa transform
             --replace-annot %self_:%self,%metadata_string:%version_20200601_tzip_b916f32_noops_dbg_contract
             _fa2_noops_dbg_contract/multi_asset_compiled.tz fa2_noops_dbg_contract.tz)
      )
)))))))
))

(rule
   (target fa2_tmp_noops_dbg_contract.ml)
   (deps fa2_noops_dbg_contract.tz)
   (action (progn
     (run flextesa ocaml --integer-types "" %{deps} %{target})
)))
(rule
   (target fa2_interface_noops_dbg_contract.ml)
   (deps fa2_tmp_noops_dbg_contract.ml)
   (action
     (with-stdout-to %{target}
       (progn
        (cat fa2_tmp_noops_dbg_contract.ml)
        (echo "\n\nlet storage_of_address addr = Storage.make                ~version_20200601_tzip_b916f32_noops_dbg_contract:M_unit.Unit                ~administrator:M_address.(Raw_b58 addr)                ~all_tokens:M_nat.(Int 0)                ~ledger:[] ~paused:M_bool.False                 ~operators:[]                 ~tokens:[]\n\n")
       )
     )
   )
)

(rule
  (target fa2_noops_dbg_contract-simulation-log.txt)
  (deps multi_asset.py fa2_contract_noops_dbg_contract.ml)
  (action
(setenv debug_mode "true"
 (setenv carthage_pairs "true"
  (setenv force_layouts "true"
   (setenv support_operator "false"
    (setenv assume_consecutive_token_ids "true"
     (setenv add_mutez_transfer "false"
       (setenv single_asset "false"
    (progn
       (with-accepted-exit-codes (or 0 1) ;; We allow failures for now
         (run SmartPy.sh test multi_asset.py _fa2_simulation_noops_dbg_contract))
       (with-stdout-to %{target} (echo "Test noops_dbg_contract"))
       (with-accepted-exit-codes (or 0 1) ;; We allow failures for now
         (run mv
         _fa2_simulation_noops_dbg_contract/interpreted-scenario/scenario-interpreter-log.txt
          %{target} ))
    )
)))))))
))
;; Name: single_contract
;; dbg    : false
;; capa   : true
;; layout : true
;; ops    : true
;; toknat : true
;; mutran : false
;; single : true

(rule
  (target fa2_code_single_contract.ml)
  (deps fa2_single_contract.tz)
  (action
    (with-stdout-to %{target}
      (progn
       (echo "let code = {letshopenobodyusesthisstring|")
       (run cat fa2_single_contract.tz)
       (echo "|letshopenobodyusesthisstring}")
))))

(rule
  (target fa2_contract_single_contract.ml)
  (action
    (with-stdout-to %{target}
     (progn
       (echo "let name = \"single_contract\"\n")
       (echo "let metadata_string = \"version_20200601_tzip_b916f32_single_contract\"\n")
       (echo "let description = \"The default for single-asset\"\n")
       (echo "let switches = Sigs.Switches.{\n")
       (echo "debug = false\n")
       (echo "; carthage_pairs = true\n")
       (echo "; force_layouts = true\n")
       (echo "; support_operator = true\n")
       (echo "; assume_consecutive_token_ids = true\n")
       (echo "; add_mutez_transfer = false\n")
       (echo "; single_asset = true\n")
       (echo "}\n")
       (echo "let code = Fa2_code_single_contract.code\n")
     )
)))

(rule
  (targets fa2_single_contract.tz _fa2_intermediate_single_contract.tz)
  (deps multi_asset.py fa2_contract_single_contract.ml)
  (action
(setenv debug_mode "false"
 (setenv carthage_pairs "true"
  (setenv force_layouts "true"
   (setenv support_operator "true"
    (setenv assume_consecutive_token_ids "true"
     (setenv add_mutez_transfer "false"
       (setenv single_asset "true"
      (progn
        (run SmartPy.sh compile-smartpy-class multi_asset.py
             "FA2(sp.address(\"tz1L1bypLzuxGHmx3d6bHFJ2WCi4ZDbocSCA\"))"
             _fa2_single_contract)
        (run cp  _fa2_single_contract/multi_asset_compiled.tz _fa2_intermediate_single_contract.tz)
        (run flextesa transform
             --replace-annot %self_:%self,%metadata_string:%version_20200601_tzip_b916f32_single_contract
             _fa2_single_contract/multi_asset_compiled.tz fa2_single_contract.tz)
      )
)))))))
))

(rule
   (target fa2_tmp_single_contract.ml)
   (deps fa2_single_contract.tz)
   (action (progn
     (run flextesa ocaml --integer-types "" %{deps} %{target})
)))
(rule
   (target fa2_interface_single_contract.ml)
   (deps fa2_tmp_single_contract.ml)
   (action
     (with-stdout-to %{target}
       (progn
        (cat fa2_tmp_single_contract.ml)
        (echo "\n\nlet storage_of_address addr = Storage.make                ~version_20200601_tzip_b916f32_single_contract:M_unit.Unit                ~administrator:M_address.(Raw_b58 addr)                ~all_tokens:M_nat.(Int 0)                ~ledger:M_big_map.(List []) ~paused:M_bool.False                 ~operators:M_big_map.(List [])                 ~tokens:M_big_map.(List [])\n\n")
       )
     )
   )
)

(rule
  (target fa2_single_contract-simulation-log.txt)
  (deps multi_asset.py fa2_contract_single_contract.ml)
  (action
(setenv debug_mode "false"
 (setenv carthage_pairs "true"
  (setenv force_layouts "true"
   (setenv support_operator "true"
    (setenv assume_consecutive_token_ids "true"
     (setenv add_mutez_transfer "false"
       (setenv single_asset "true"
    (progn
       (with-accepted-exit-codes (or 0 1) ;; We allow failures for now
         (run SmartPy.sh test multi_asset.py _fa2_simulation_single_contract))
       (with-stdout-to %{target} (echo "Test single_contract"))
       (with-accepted-exit-codes (or 0 1) ;; We allow failures for now
         (run mv
         _fa2_simulation_single_contract/interpreted-scenario/scenario-interpreter-log.txt
          %{target} ))
    )
)))))))
))
;; Name: single_mutran_contract
;; dbg    : false
;; capa   : true
;; layout : true
;; ops    : true
;; toknat : true
;; mutran : true
;; single : true

(rule
  (target fa2_code_single_mutran_contract.ml)
  (deps fa2_single_mutran_contract.tz)
  (action
    (with-stdout-to %{target}
      (progn
       (echo "let code = {letshopenobodyusesthisstring|")
       (run cat fa2_single_mutran_contract.tz)
       (echo "|letshopenobodyusesthisstring}")
))))

(rule
  (target fa2_contract_single_mutran_contract.ml)
  (action
    (with-stdout-to %{target}
     (progn
       (echo "let name = \"single_mutran_contract\"\n")
       (echo "let metadata_string = \"version_20200601_tzip_b916f32_single_mutran_contract\"\n")
       (echo "let description = \"The single-asset with mutez transfer entry-point\"\n")
       (echo "let switches = Sigs.Switches.{\n")
       (echo "debug = false\n")
       (echo "; carthage_pairs = true\n")
       (echo "; force_layouts = true\n")
       (echo "; support_operator = true\n")
       (echo "; assume_consecutive_token_ids = true\n")
       (echo "; add_mutez_transfer = true\n")
       (echo "; single_asset = true\n")
       (echo "}\n")
       (echo "let code = Fa2_code_single_mutran_contract.code\n")
     )
)))

(rule
  (targets fa2_single_mutran_contract.tz _fa2_intermediate_single_mutran_contract.tz)
  (deps multi_asset.py fa2_contract_single_mutran_contract.ml)
  (action
(setenv debug_mode "false"
 (setenv carthage_pairs "true"
  (setenv force_layouts "true"
   (setenv support_operator "true"
    (setenv assume_consecutive_token_ids "true"
     (setenv add_mutez_transfer "true"
       (setenv single_asset "true"
      (progn
        (run SmartPy.sh compile-smartpy-class multi_asset.py
             "FA2(sp.address(\"tz1L1bypLzuxGHmx3d6bHFJ2WCi4ZDbocSCA\"))"
             _fa2_single_mutran_contract)
        (run cp  _fa2_single_mutran_contract/multi_asset_compiled.tz _fa2_intermediate_single_mutran_contract.tz)
        (run flextesa transform
             --replace-annot %self_:%self,%metadata_string:%version_20200601_tzip_b916f32_single_mutran_contract
             _fa2_single_mutran_contract/multi_asset_compiled.tz fa2_single_mutran_contract.tz)
      )
)))))))
))

(rule
   (target fa2_tmp_single_mutran_contract.ml)
   (deps fa2_single_mutran_contract.tz)
   (action (progn
     (run flextesa ocaml --integer-types "" %{deps} %{target})
)))
(rule
   (target fa2_interface_single_mutran_contract.ml)
   (deps fa2_tmp_single_mutran_contract.ml)
   (action
     (with-stdout-to %{target}
       (progn
        (cat fa2_tmp_single_mutran_contract.ml)
        (echo "\n\nlet storage_of_address addr = Storage.make                ~version_20200601_tzip_b916f32_single_mutran_contract:M_unit.Unit                ~administrator:M_address.(Raw_b58 addr)                ~all_tokens:M_nat.(Int 0)                ~ledger:M_big_map.(List []) ~paused:M_bool.False                 ~operators:M_big_map.(List [])                 ~tokens:M_big_map.(List [])\n\n")
       )
     )
   )
)

(rule
  (target fa2_single_mutran_contract-simulation-log.txt)
  (deps multi_asset.py fa2_contract_single_mutran_contract.ml)
  (action
(setenv debug_mode "false"
 (setenv carthage_pairs "true"
  (setenv force_layouts "true"
   (setenv support_operator "true"
    (setenv assume_consecutive_token_ids "true"
     (setenv add_mutez_transfer "true"
       (setenv single_asset "true"
    (progn
       (with-accepted-exit-codes (or 0 1) ;; We allow failures for now
         (run SmartPy.sh test multi_asset.py _fa2_simulation_single_mutran_contract))
       (with-stdout-to %{target} (echo "Test single_mutran_contract"))
       (with-accepted-exit-codes (or 0 1) ;; We allow failures for now
         (run mv
         _fa2_simulation_single_mutran_contract/interpreted-scenario/scenario-interpreter-log.txt
          %{target} ))
    )
)))))))
))

(rule
  (target fa2_contracts.ml)
  (deps  fa2_interface_contract.ml fa2_contract_contract.ml fa2_interface_dbg_contract.ml fa2_contract_dbg_contract.ml fa2_interface_baby_contract.ml fa2_contract_baby_contract.ml fa2_interface_nolay_contract.ml fa2_contract_nolay_contract.ml fa2_interface_mutran_contract.ml fa2_contract_mutran_contract.ml fa2_interface_tokset_contract.ml fa2_contract_tokset_contract.ml fa2_interface_noops_contract.ml fa2_contract_noops_contract.ml fa2_interface_noops_dbg_contract.ml fa2_contract_noops_dbg_contract.ml fa2_interface_single_contract.ml fa2_contract_single_contract.ml fa2_interface_single_mutran_contract.ml fa2_contract_single_mutran_contract.ml)
  (action
    (with-stdout-to %{target}
     (echo "
let version_string = \"version_20200601_tzip_b916f32\"
let all = [
  (module Fa2_contract_contract : Sigs.CONTRACT),
  (module Fa2_interface_contract : Sigs.CONTRACT_INTERFACE);
  (module Fa2_contract_dbg_contract : Sigs.CONTRACT),
  (module Fa2_interface_dbg_contract : Sigs.CONTRACT_INTERFACE);
  (module Fa2_contract_baby_contract : Sigs.CONTRACT),
  (module Fa2_interface_baby_contract : Sigs.CONTRACT_INTERFACE);
  (module Fa2_contract_nolay_contract : Sigs.CONTRACT),
  (module Fa2_interface_nolay_contract : Sigs.CONTRACT_INTERFACE);
  (module Fa2_contract_mutran_contract : Sigs.CONTRACT),
  (module Fa2_interface_mutran_contract : Sigs.CONTRACT_INTERFACE);
  (module Fa2_contract_tokset_contract : Sigs.CONTRACT),
  (module Fa2_interface_tokset_contract : Sigs.CONTRACT_INTERFACE);
  (module Fa2_contract_noops_contract : Sigs.CONTRACT),
  (module Fa2_interface_noops_contract : Sigs.CONTRACT_INTERFACE);
  (module Fa2_contract_noops_dbg_contract : Sigs.CONTRACT),
  (module Fa2_interface_noops_dbg_contract : Sigs.CONTRACT_INTERFACE);
  (module Fa2_contract_single_contract : Sigs.CONTRACT),
  (module Fa2_interface_single_contract : Sigs.CONTRACT_INTERFACE);
  (module Fa2_contract_single_mutran_contract : Sigs.CONTRACT),
  (module Fa2_interface_single_mutran_contract : Sigs.CONTRACT_INTERFACE);
]
"))))

;; high-level aliases
(alias (name contracts)
  (deps  fa2_contract.tz fa2_dbg_contract.tz fa2_baby_contract.tz fa2_nolay_contract.tz fa2_mutran_contract.tz fa2_tokset_contract.tz fa2_noops_contract.tz fa2_noops_dbg_contract.tz fa2_single_contract.tz fa2_single_mutran_contract.tz ))
(alias (name simulations)
  (deps  fa2_contract-simulation-log.txt fa2_dbg_contract-simulation-log.txt fa2_baby_contract-simulation-log.txt fa2_nolay_contract-simulation-log.txt fa2_mutran_contract-simulation-log.txt fa2_tokset_contract-simulation-log.txt fa2_noops_contract-simulation-log.txt fa2_noops_dbg_contract-simulation-log.txt fa2_single_contract-simulation-log.txt fa2_single_mutran_contract-simulation-log.txt ))
