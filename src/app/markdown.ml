open! Base
open! Common
open Io.Monad_infix

module Item = struct
  type subset = int * int [@@deriving show, eq]

  type command_display =
    [ `Code_block of
      [`Command | `Stdout of subset option | `Stderr of subset option] list
    | `Inline_stdout
    | `Quoted_stdout
    | `None ]
  [@@deriving show, eq]

  type t =
    | Paragraph of string list
    | Code_block of {lang: string; content: string list}
    | Blocks of t list
    | Section of string * t
    | Command_bloc of
        { command: string
        ; hidden_setup: string list
        ; failure_expected: bool
        ; how: command_display
        ; output: (string * string) option }
  [@@deriving show, eq]
end

type t = {header: (string * string) list; items: Item.t list [@main]}
[@@deriving make, show, eq]

open Item

let p l = Paragraph l
let section title l = Section (title, Blocks l)

let command_bloc ?(hidden_setup = []) ?(failure_expected = false)
    ?(how = `Code_block [`Command; `Stdout None]) ?output command =
  Command_bloc {command; how; output; failure_expected; hidden_setup}

let silent_command c = command_bloc ~how:`None ?output:None c
let pf fmt = Fmt.kstr (fun s -> p (String.split ~on:'\n' s)) fmt
let code_block ?(lang = "") content = Code_block {lang; content}

let par s =
  Fmt.kstr (fun s -> p (String.split ~on:'\n' s)) "%a" Fmt.(box text) s

let itemize l =
  p (List.map l ~f:(fun line -> Fmt.str "* %a" Fmt.(box text) line))

let ( %% ) a b = a ^ " " ^ b
let ( % ) a b = a ^ b
let uri u = Fmt.str "<%s>" u

let command_and_stderr : Item.command_display =
  `Code_block [`Command; `Stderr None]

let command_and_stdall : Item.command_display =
  `Code_block [`Command; `Stdout None; `Stderr None]

let print ppf {header; items} =
  let open Fmt in
  let rec pp_item ?(depth = 2) ppf =
    let continue = pp_item ~depth in
    function
    | Paragraph l ->
        List.iter l ~f:(fun s -> pf ppf "@.%s" s) ;
        pf ppf "@."
    | Blocks l -> List.iter l ~f:(continue ppf)
    | Code_block {lang; content} ->
        pf ppf "@.```%s" lang ;
        List.iter content ~f:(pf ppf "@.%s") ;
        pf ppf "@.```@."
    | Command_bloc {how= `None; _} -> ()
    | Command_bloc {how= `Inline_stdout | `Quoted_stdout; output= None; _} -> ()
    | Command_bloc {how= `Inline_stdout; command; output= Some (o, _); _} ->
        pf ppf "@.<!-- output of %s -->" command ;
        pf ppf "@.%a" lines o ;
        ()
    | Command_bloc {how= `Quoted_stdout; command; output= Some (o, _); _} ->
        pf ppf "@.<!-- output of %s --><blockquote>" command ;
        pf ppf "@.%a</blockquote>" lines o ;
        ()
    | Command_bloc
        { command
        ; how= `Code_block how
        ; output
        ; failure_expected= _
        ; hidden_setup= _ } ->
        let c_lines = String.split ~on:'\n' command in
        let c_out, c_err =
          match output with
          | None -> ([], [])
          | Some (o, e) ->
              let splean s =
                String.split ~on:'\n' s
                |> List.filter ~f:(fun s ->
                       not
                         (String.for_all s ~f:(function
                           | ' ' | '\n' | '\t' -> true
                           | _ -> false))) in
              (splean o, splean e) in
        pf ppf "@.```sh" ;
        let subsetize l = function
          | None -> l
          | Some (h, t) ->
              List.take l h @ [""; "..."; ""] @ List.drop l (List.length l - t)
        in
        let pp_block line_prefix =
          vbox ~indent:0 (list ~sep:cut (const string line_prefix ++ string))
        in
        let pp_cmd = pp_block "" in
        List.iter how ~f:(function
          | `Command -> pf ppf "@. $ %a" pp_cmd c_lines
          | `Stdout subset ->
              pf ppf "@.%a" (pp_block "┃ ") (subsetize c_out subset)
          | `Stderr subset ->
              pf ppf "@.%a" (pp_block "‖ ") (subsetize c_err subset)) ;
        pf ppf "@.```@."
    | Section (title, l) ->
        pf ppf "@.%s %s@." (String.make depth '#') title ;
        pp_item ~depth:(depth + 1) ppf l in
  ( match header with
  | [] -> ()
  | _ ->
      pf ppf "---@." ;
      List.iter header ~f:(fun (k, v) -> pf ppf "%s: %s@." k v) ;
      pf ppf "---@." ) ;
  List.iter items ~f:(fun i -> pp_item ppf i) ;
  ()

let subroot_counter = ref 0

let complete ?(fake_commands = false) state t =
  let wrap_command ~hidden_setup s =
    Int.incr subroot_counter ;
    let fake cmd =
      Fmt.str "function %s { echo \"calling %s $*\" ; }  " cmd cmd in
    let fakes =
      String.concat ~sep:"\n"
        (List.map ~f:fake ["fatoo"; "wget"; "tezos-client"; "flextesa"]) in
    let script =
      Fmt.str
        "export fatoo_root_path=%s/subroot-%04d\n\
         mkdir -p %s/playground\n\
        \ cd %s/playground\n\
         %s\n\
         %s\n\
         %s\n"
        (State.root_path state) !subroot_counter (State.root_path state)
        (State.root_path state)
        (if fake_commands then fakes else "")
        (String.concat ~sep:"\n" hidden_setup)
        s in
    Fmt.str "bash -c %s" (Path.quote script) in
  let rec fill_one =
    let open Item in
    function
    | Command_bloc
        ({command; hidden_setup; how= _; output= None; failure_expected} as cb)
      ->
        (* let output = Fmt.str "TOTOTODODODO\nOutput of %s\nTODO" command in *)
        Command.run state
          (wrap_command ~hidden_setup command)
          ~succeed:(not failure_expected)
        >>= fun cmd ->
        let err = Command.Command_result.get_stderr state cmd in
        let out = Command.Command_result.get_stdout state cmd in
        Io.return (Command_bloc {cb with output= Some (out, err)})
    | Blocks l -> fill l >>= fun ll -> Io.return (Blocks ll)
    | Section (t, o) -> fill_one o >>= fun f -> Io.return (Section (t, f))
    | (Code_block _ | Command_bloc _ | Paragraph _) as e -> Io.return e
  and fill list =
    List.fold list ~init:(Io.return []) ~f:(fun prevm i ->
        prevm
        >>= fun prev -> fill_one i >>= fun filled -> Io.return (filled :: prev))
    >>| List.rev in
  fill t.items >>= fun filled_items -> Io.return {t with items= filled_items}
